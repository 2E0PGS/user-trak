﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using UserTrak.Data.Models;

namespace UserTrak.Data.Repositories
{
    // Based on: https://bitbucket.org/2E0PGS/always-watching-bot/src/master/AlwaysWatchingBot.Data
    public class WorkLogRepository : BaseRespository
    {
        public void CreateWorkLogAsync(WorkLogModel workLogModel)
        {
            using (SQLiteConnection sqliteConnection = GetSQLiteConnection())
            {
                sqliteConnection.Open();
                string query = "INSERT INTO WorkLog (DateTimeStamp, Status, WorkType, MinsToConsiderIdle, LastActiveWindow) Values (@DateTimeStamp, @Status, @WorkType, @MinsToConsiderIdle, @LastActiveWindow)";
                int rows = sqliteConnection.Execute(query, workLogModel);
                sqliteConnection.Close();
                if (rows != 1)
                {
                    throw new Exception("Effected row count was an unexpected: " + rows);
                }
            }
        }
    }
}
