﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace UserTrak.Data.Repositories
{
    // Based on: https://bitbucket.org/2E0PGS/always-watching-bot/src/master/AlwaysWatchingBot.Data
    public class BaseRespository
    {
        public static string FilePath { get; set; }

        public static SQLiteConnection GetSQLiteConnection()
        {
            if (string.IsNullOrEmpty(FilePath))
            {
                throw new ArgumentNullException("FilePath is empty. Did you setup this at startup?");
            }
            return new SQLiteConnection("Data Source=" + FilePath + "; Version=3;");
        }

        public static bool SetupDbIfRequired()
        {
            if (!File.Exists(FilePath))
            {
                SQLiteConnection.CreateFile(FilePath);

                // Setup Messages table.
                using (SQLiteConnection sqliteConnection = GetSQLiteConnection())
                {
                    sqliteConnection.Open();
                    string query = "CREATE TABLE WorkLog(DateTimeStamp datetime, Status VARCHAR(100), WorkType int, MinsToConsiderIdle int, LastActivewindow VARCHAR(100))";
                    sqliteConnection.Execute(query);
                    sqliteConnection.Close();
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
