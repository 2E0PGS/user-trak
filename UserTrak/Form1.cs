﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace UserTrak
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")] // Do we need set last error? 
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")] // User32.dll or user32.dll?
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        [DllImport("user32.dll")]
        public static extern bool GetLastInputInfo(ref LastInputInfo noRefs);

        public struct LastInputInfo
        {
            public UInt32 cbSize;
            public UInt32 dwTime;
        }

        public Form1()
        {
            InitializeComponent();
            Data.Repositories.BaseRespository.FilePath = "user-trak.sqlite"; // TODO: Move to config.
            if (Data.Repositories.BaseRespository.SetupDbIfRequired())
            {
                System.Diagnostics.Debug.WriteLine("Created DB as SQLite file was missing.");
            }
        }

        private long GetIdleTimeInMs()
        {
            LastInputInfo _lastInputInfo = new LastInputInfo();
            _lastInputInfo.cbSize = (uint)Marshal.SizeOf(_lastInputInfo);
            if (!GetLastInputInfo(ref _lastInputInfo))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return (Environment.TickCount - _lastInputInfo.dwTime);
        }

        private int GetIdleTimeInSeconds()
        {
            return (int)(GetIdleTimeInMs() / 1000);
        }

        private int GetIdleTimeInMins()
        {
            return (GetIdleTimeInSeconds() / 60);
        }

        private string GetActiveWindow()
        {
            int count = 256;
            StringBuilder text = new StringBuilder(count);
            IntPtr hWnd = GetForegroundWindow();
            if (GetWindowText(hWnd, text, count) > 0)
            {
                return text.ToString();
            }
            else
            {
                return "";
            }
        }

        private void btnRunning_Click(object sender, EventArgs e)
        {
            if (btnRunning.Text == "Stopped")
            {
                timer1.Enabled = true;
                btnRunning.Text = "Running";
            }
            else
            {
                timer1.Enabled = false;
                btnRunning.Text = "Stopped";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int idleMins = GetIdleTimeInMins();
            lblRealTimeIdleMins.Text = idleMins.ToString(); // Realtime change.

            if (idleMins == 0)
            {
                if (lblRealTimeUserStatus.Text != "Active") // Realtime change.
                {
                    pbGlobalStatus.BackColor = Color.Green;
                    lblRealTimeUserStatus.Text = "Active";
                }

                if (lblUserStatus.Text != "Active") // State change.
                {
                    lblIdleMins.Text = "0"; // Reset
                    lblUserStatus.Text = "Active"; // Reset
                    lblLastActiveWindowAtStateChange.Text = GetActiveWindow();
                    LogStateChange(lblUserStatus.Text, dtpMinsToConsiderIdle.Value.Minute, lblLastActiveWindowAtStateChange.Text);
                }
            }
            else if (idleMins > dtpMinsToConsiderIdle.Value.Minute)
            {
                if (lblUserStatus.Text != "Idle") // State change.
                {
                    pbGlobalStatus.BackColor = Color.Red;
                    lblUserStatus.Text = "Idle";
                    lblLastActiveWindowAtStateChange.Text = GetActiveWindow();
                    LogStateChange(lblUserStatus.Text, dtpMinsToConsiderIdle.Value.Minute, lblLastActiveWindowAtStateChange.Text);
                }
                lblIdleMins.Text = (idleMins - dtpMinsToConsiderIdle.Value.Minute).ToString();
            }
            else
            {
                if (lblRealTimeUserStatus.Text != "Idle") // Realtime change.
                {
                    pbGlobalStatus.BackColor = Color.Yellow;
                    lblRealTimeUserStatus.Text = "Idle";
                }
            }
        }

        private int GetWorkType()
        {
            DateTime dateTimeNow = DateTime.Now;

            if (dateTimeNow.DayOfWeek == DayOfWeek.Monday)
            {
                if (!cbMonday.Checked)
                {
                    return 1;
                }
            }
            else if (dateTimeNow.DayOfWeek == DayOfWeek.Tuesday)
            {
                if (!cbTuesday.Checked)
                {
                    return 1;
                }
            }
            else if (dateTimeNow.DayOfWeek == DayOfWeek.Wednesday)
            {
                if (!cbWednesday.Checked)
                {
                    return 1;
                }
            }
            else if (dateTimeNow.DayOfWeek == DayOfWeek.Thursday)
            {
                if (!cbThursday.Checked)
                {
                    return 1;
                }
            }
            else if (dateTimeNow.DayOfWeek == DayOfWeek.Friday)
            {
                if (!cbFriday.Checked)
                {
                    return 1;
                }
            }

            if (dateTimeNow.TimeOfDay < dtpWorkDayStartTime.Value.TimeOfDay)
            {
                return 1;
            }
            else if (dateTimeNow.TimeOfDay > dtpWorkDayEndTime.Value.TimeOfDay)
            {
                return 1;
            }

            return 0; // Inside work hours.
        }

        private void LogStateChange(string status, int minsToConsiderIdle, string lastActiveWindow)
        {
            int workType = GetWorkType();

            System.Diagnostics.Debug.WriteLine(
                " Status: " + status + 
                " WorkType: " + workType.ToString() + 
                " MinsToConsiderIdle: " + minsToConsiderIdle.ToString() +
                " LastActiveWindow: " + lastActiveWindow);

            var workLogRepo = new Data.Repositories.WorkLogRepository();
            var workLogModel = new Data.Models.WorkLogModel();
            workLogModel.DateTimeStamp = DateTime.Now;
            workLogModel.Status = status;
            workLogModel.WorkType = workType;
            workLogModel.MinsToConsiderIdle = minsToConsiderIdle;
            workLogModel.LastActiveWindow = lastActiveWindow;

            workLogRepo.CreateWorkLogAsync(workLogModel);
        }
    }
}
