﻿namespace UserTrak
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRealTimeUserStatus = new System.Windows.Forms.Label();
            this.cbMonday = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTuesday = new System.Windows.Forms.CheckBox();
            this.cbWednesday = new System.Windows.Forms.CheckBox();
            this.cbThursday = new System.Windows.Forms.CheckBox();
            this.cbFriday = new System.Windows.Forms.CheckBox();
            this.cbSaturday = new System.Windows.Forms.CheckBox();
            this.cbSunday = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpWorkDayStartTime = new System.Windows.Forms.DateTimePicker();
            this.dtpWorkDayEndTime = new System.Windows.Forms.DateTimePicker();
            this.btnRunning = new System.Windows.Forms.Button();
            this.pbGlobalStatus = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpMinsToConsiderIdle = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.lblLastActiveWindowAtStateChange = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblRealTimeIdleMins = new System.Windows.Forms.Label();
            this.gbRealtimeInfo = new System.Windows.Forms.GroupBox();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.gbTrackingInfo = new System.Windows.Forms.GroupBox();
            this.lblUserStatus = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblIdleMins = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pbGlobalStatus)).BeginInit();
            this.gbRealtimeInfo.SuspendLayout();
            this.gbSettings.SuspendLayout();
            this.gbTrackingInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(20, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Realtime user status:";
            // 
            // lblRealTimeUserStatus
            // 
            this.lblRealTimeUserStatus.AutoSize = true;
            this.lblRealTimeUserStatus.ForeColor = System.Drawing.Color.Yellow;
            this.lblRealTimeUserStatus.Location = new System.Drawing.Point(128, 27);
            this.lblRealTimeUserStatus.Name = "lblRealTimeUserStatus";
            this.lblRealTimeUserStatus.Size = new System.Drawing.Size(53, 13);
            this.lblRealTimeUserStatus.TabIndex = 1;
            this.lblRealTimeUserStatus.Text = "Unknown";
            // 
            // cbMonday
            // 
            this.cbMonday.AutoSize = true;
            this.cbMonday.Checked = true;
            this.cbMonday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMonday.ForeColor = System.Drawing.Color.Yellow;
            this.cbMonday.Location = new System.Drawing.Point(73, 15);
            this.cbMonday.Name = "cbMonday";
            this.cbMonday.Size = new System.Drawing.Size(64, 17);
            this.cbMonday.TabIndex = 2;
            this.cbMonday.Text = "Monday";
            this.cbMonday.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Work days:";
            // 
            // cbTuesday
            // 
            this.cbTuesday.AutoSize = true;
            this.cbTuesday.Checked = true;
            this.cbTuesday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTuesday.ForeColor = System.Drawing.Color.Yellow;
            this.cbTuesday.Location = new System.Drawing.Point(143, 16);
            this.cbTuesday.Name = "cbTuesday";
            this.cbTuesday.Size = new System.Drawing.Size(67, 17);
            this.cbTuesday.TabIndex = 4;
            this.cbTuesday.Text = "Tuesday";
            this.cbTuesday.UseVisualStyleBackColor = true;
            // 
            // cbWednesday
            // 
            this.cbWednesday.AutoSize = true;
            this.cbWednesday.Checked = true;
            this.cbWednesday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWednesday.ForeColor = System.Drawing.Color.Yellow;
            this.cbWednesday.Location = new System.Drawing.Point(216, 16);
            this.cbWednesday.Name = "cbWednesday";
            this.cbWednesday.Size = new System.Drawing.Size(83, 17);
            this.cbWednesday.TabIndex = 5;
            this.cbWednesday.Text = "Wednesday";
            this.cbWednesday.UseVisualStyleBackColor = true;
            // 
            // cbThursday
            // 
            this.cbThursday.AutoSize = true;
            this.cbThursday.Checked = true;
            this.cbThursday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbThursday.ForeColor = System.Drawing.Color.Yellow;
            this.cbThursday.Location = new System.Drawing.Point(305, 16);
            this.cbThursday.Name = "cbThursday";
            this.cbThursday.Size = new System.Drawing.Size(70, 17);
            this.cbThursday.TabIndex = 6;
            this.cbThursday.Text = "Thursday";
            this.cbThursday.UseVisualStyleBackColor = true;
            // 
            // cbFriday
            // 
            this.cbFriday.AutoSize = true;
            this.cbFriday.Checked = true;
            this.cbFriday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFriday.ForeColor = System.Drawing.Color.Yellow;
            this.cbFriday.Location = new System.Drawing.Point(381, 15);
            this.cbFriday.Name = "cbFriday";
            this.cbFriday.Size = new System.Drawing.Size(54, 17);
            this.cbFriday.TabIndex = 7;
            this.cbFriday.Text = "Friday";
            this.cbFriday.UseVisualStyleBackColor = true;
            // 
            // cbSaturday
            // 
            this.cbSaturday.AutoSize = true;
            this.cbSaturday.ForeColor = System.Drawing.Color.Yellow;
            this.cbSaturday.Location = new System.Drawing.Point(441, 16);
            this.cbSaturday.Name = "cbSaturday";
            this.cbSaturday.Size = new System.Drawing.Size(68, 17);
            this.cbSaturday.TabIndex = 8;
            this.cbSaturday.Text = "Saturday";
            this.cbSaturday.UseVisualStyleBackColor = true;
            // 
            // cbSunday
            // 
            this.cbSunday.AutoSize = true;
            this.cbSunday.ForeColor = System.Drawing.Color.Yellow;
            this.cbSunday.Location = new System.Drawing.Point(515, 16);
            this.cbSunday.Name = "cbSunday";
            this.cbSunday.Size = new System.Drawing.Size(62, 17);
            this.cbSunday.TabIndex = 9;
            this.cbSunday.Text = "Sunday";
            this.cbSunday.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.ForeColor = System.Drawing.Color.Yellow;
            this.label3.Location = new System.Drawing.Point(6, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Work day start time:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(4, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Work day end time:";
            // 
            // dtpWorkDayStartTime
            // 
            this.dtpWorkDayStartTime.CustomFormat = "";
            this.dtpWorkDayStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpWorkDayStartTime.Location = new System.Drawing.Point(113, 48);
            this.dtpWorkDayStartTime.Name = "dtpWorkDayStartTime";
            this.dtpWorkDayStartTime.ShowUpDown = true;
            this.dtpWorkDayStartTime.Size = new System.Drawing.Size(81, 20);
            this.dtpWorkDayStartTime.TabIndex = 12;
            this.dtpWorkDayStartTime.Value = new System.DateTime(2020, 10, 26, 9, 0, 0, 0);
            // 
            // dtpWorkDayEndTime
            // 
            this.dtpWorkDayEndTime.CustomFormat = "";
            this.dtpWorkDayEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpWorkDayEndTime.Location = new System.Drawing.Point(113, 85);
            this.dtpWorkDayEndTime.Name = "dtpWorkDayEndTime";
            this.dtpWorkDayEndTime.ShowUpDown = true;
            this.dtpWorkDayEndTime.Size = new System.Drawing.Size(81, 20);
            this.dtpWorkDayEndTime.TabIndex = 13;
            this.dtpWorkDayEndTime.Value = new System.DateTime(2020, 10, 26, 16, 30, 0, 0);
            // 
            // btnRunning
            // 
            this.btnRunning.ForeColor = System.Drawing.Color.Black;
            this.btnRunning.Location = new System.Drawing.Point(513, 344);
            this.btnRunning.Name = "btnRunning";
            this.btnRunning.Size = new System.Drawing.Size(75, 23);
            this.btnRunning.TabIndex = 14;
            this.btnRunning.Text = "Stopped";
            this.btnRunning.UseVisualStyleBackColor = true;
            this.btnRunning.Click += new System.EventHandler(this.btnRunning_Click);
            // 
            // pbGlobalStatus
            // 
            this.pbGlobalStatus.BackColor = System.Drawing.Color.Gray;
            this.pbGlobalStatus.Location = new System.Drawing.Point(599, 344);
            this.pbGlobalStatus.Name = "pbGlobalStatus";
            this.pbGlobalStatus.Size = new System.Drawing.Size(25, 24);
            this.pbGlobalStatus.TabIndex = 16;
            this.pbGlobalStatus.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Yellow;
            this.label5.Location = new System.Drawing.Point(6, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Mins to consider idle:";
            // 
            // dtpMinsToConsiderIdle
            // 
            this.dtpMinsToConsiderIdle.CustomFormat = "mm";
            this.dtpMinsToConsiderIdle.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMinsToConsiderIdle.Location = new System.Drawing.Point(113, 128);
            this.dtpMinsToConsiderIdle.Name = "dtpMinsToConsiderIdle";
            this.dtpMinsToConsiderIdle.ShowUpDown = true;
            this.dtpMinsToConsiderIdle.Size = new System.Drawing.Size(81, 20);
            this.dtpMinsToConsiderIdle.TabIndex = 19;
            this.dtpMinsToConsiderIdle.Value = new System.DateTime(2020, 10, 26, 0, 29, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Yellow;
            this.label6.Location = new System.Drawing.Point(9, 355);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(178, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Last active window at state change:";
            // 
            // lblLastActiveWindowAtStateChange
            // 
            this.lblLastActiveWindowAtStateChange.AutoSize = true;
            this.lblLastActiveWindowAtStateChange.ForeColor = System.Drawing.Color.Yellow;
            this.lblLastActiveWindowAtStateChange.Location = new System.Drawing.Point(193, 355);
            this.lblLastActiveWindowAtStateChange.Name = "lblLastActiveWindowAtStateChange";
            this.lblLastActiveWindowAtStateChange.Size = new System.Drawing.Size(53, 13);
            this.lblLastActiveWindowAtStateChange.TabIndex = 21;
            this.lblLastActiveWindowAtStateChange.Text = "Unknown";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Yellow;
            this.label7.Location = new System.Drawing.Point(20, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Realtime idle mins:";
            // 
            // lblRealTimeIdleMins
            // 
            this.lblRealTimeIdleMins.AutoSize = true;
            this.lblRealTimeIdleMins.ForeColor = System.Drawing.Color.Yellow;
            this.lblRealTimeIdleMins.Location = new System.Drawing.Point(128, 57);
            this.lblRealTimeIdleMins.Name = "lblRealTimeIdleMins";
            this.lblRealTimeIdleMins.Size = new System.Drawing.Size(13, 13);
            this.lblRealTimeIdleMins.TabIndex = 23;
            this.lblRealTimeIdleMins.Text = "0";
            // 
            // gbRealtimeInfo
            // 
            this.gbRealtimeInfo.Controls.Add(this.label1);
            this.gbRealtimeInfo.Controls.Add(this.lblRealTimeIdleMins);
            this.gbRealtimeInfo.Controls.Add(this.lblRealTimeUserStatus);
            this.gbRealtimeInfo.Controls.Add(this.label7);
            this.gbRealtimeInfo.ForeColor = System.Drawing.Color.Yellow;
            this.gbRealtimeInfo.Location = new System.Drawing.Point(12, 211);
            this.gbRealtimeInfo.Name = "gbRealtimeInfo";
            this.gbRealtimeInfo.Size = new System.Drawing.Size(200, 100);
            this.gbRealtimeInfo.TabIndex = 24;
            this.gbRealtimeInfo.TabStop = false;
            this.gbRealtimeInfo.Text = "Realtime info";
            // 
            // gbSettings
            // 
            this.gbSettings.Controls.Add(this.label2);
            this.gbSettings.Controls.Add(this.cbMonday);
            this.gbSettings.Controls.Add(this.cbTuesday);
            this.gbSettings.Controls.Add(this.cbWednesday);
            this.gbSettings.Controls.Add(this.dtpMinsToConsiderIdle);
            this.gbSettings.Controls.Add(this.cbThursday);
            this.gbSettings.Controls.Add(this.label5);
            this.gbSettings.Controls.Add(this.cbFriday);
            this.gbSettings.Controls.Add(this.cbSaturday);
            this.gbSettings.Controls.Add(this.cbSunday);
            this.gbSettings.Controls.Add(this.label3);
            this.gbSettings.Controls.Add(this.dtpWorkDayEndTime);
            this.gbSettings.Controls.Add(this.dtpWorkDayStartTime);
            this.gbSettings.Controls.Add(this.label4);
            this.gbSettings.ForeColor = System.Drawing.Color.Yellow;
            this.gbSettings.Location = new System.Drawing.Point(12, 12);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(613, 193);
            this.gbSettings.TabIndex = 25;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Settings";
            // 
            // gbTrackingInfo
            // 
            this.gbTrackingInfo.BackColor = System.Drawing.Color.Black;
            this.gbTrackingInfo.Controls.Add(this.lblUserStatus);
            this.gbTrackingInfo.Controls.Add(this.label9);
            this.gbTrackingInfo.Controls.Add(this.lblIdleMins);
            this.gbTrackingInfo.Controls.Add(this.label8);
            this.gbTrackingInfo.ForeColor = System.Drawing.Color.Yellow;
            this.gbTrackingInfo.Location = new System.Drawing.Point(228, 211);
            this.gbTrackingInfo.Name = "gbTrackingInfo";
            this.gbTrackingInfo.Size = new System.Drawing.Size(202, 100);
            this.gbTrackingInfo.TabIndex = 26;
            this.gbTrackingInfo.TabStop = false;
            this.gbTrackingInfo.Text = "Tracker info";
            // 
            // lblUserStatus
            // 
            this.lblUserStatus.AutoSize = true;
            this.lblUserStatus.ForeColor = System.Drawing.Color.Yellow;
            this.lblUserStatus.Location = new System.Drawing.Point(83, 27);
            this.lblUserStatus.Name = "lblUserStatus";
            this.lblUserStatus.Size = new System.Drawing.Size(53, 13);
            this.lblUserStatus.TabIndex = 24;
            this.lblUserStatus.Text = "Unknown";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Yellow;
            this.label9.Location = new System.Drawing.Point(16, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "User status:";
            // 
            // lblIdleMins
            // 
            this.lblIdleMins.AutoSize = true;
            this.lblIdleMins.ForeColor = System.Drawing.Color.Yellow;
            this.lblIdleMins.Location = new System.Drawing.Point(83, 57);
            this.lblIdleMins.Name = "lblIdleMins";
            this.lblIdleMins.Size = new System.Drawing.Size(13, 13);
            this.lblIdleMins.TabIndex = 23;
            this.lblIdleMins.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Yellow;
            this.label8.Location = new System.Drawing.Point(16, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Idle mins:";
            // 
            // timer1
            // 
            this.timer1.Interval = 6000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(636, 380);
            this.Controls.Add(this.gbTrackingInfo);
            this.Controls.Add(this.gbSettings);
            this.Controls.Add(this.lblLastActiveWindowAtStateChange);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnRunning);
            this.Controls.Add(this.gbRealtimeInfo);
            this.Controls.Add(this.pbGlobalStatus);
            this.Name = "Form1";
            this.Text = "UserTrak";
            ((System.ComponentModel.ISupportInitialize)(this.pbGlobalStatus)).EndInit();
            this.gbRealtimeInfo.ResumeLayout(false);
            this.gbRealtimeInfo.PerformLayout();
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            this.gbTrackingInfo.ResumeLayout(false);
            this.gbTrackingInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRealTimeUserStatus;
        private System.Windows.Forms.CheckBox cbMonday;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbTuesday;
        private System.Windows.Forms.CheckBox cbWednesday;
        private System.Windows.Forms.CheckBox cbThursday;
        private System.Windows.Forms.CheckBox cbFriday;
        private System.Windows.Forms.CheckBox cbSaturday;
        private System.Windows.Forms.CheckBox cbSunday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpWorkDayStartTime;
        private System.Windows.Forms.DateTimePicker dtpWorkDayEndTime;
        private System.Windows.Forms.Button btnRunning;
        private System.Windows.Forms.PictureBox pbGlobalStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpMinsToConsiderIdle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblLastActiveWindowAtStateChange;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblRealTimeIdleMins;
        private System.Windows.Forms.GroupBox gbRealtimeInfo;
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.GroupBox gbTrackingInfo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblIdleMins;
        private System.Windows.Forms.Label lblUserStatus;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer timer1;
    }
}

