﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserTrak.Data.Models
{
    public class WorkLogModel
    {
        public DateTime DateTimeStamp { get; set; }
        public string Status { get; set; }
        public int WorkType { get; set; }
        public int MinsToConsiderIdle { get; set; }
        public string LastActiveWindow { get; set; }
    }
}
